class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    let hariBooking = new Date(params.tanggal);
    let i = 0;
    Car.list.forEach((car) => {
      let batasTersedia = new Date(car.availableAt);

      if (batasTersedia.toDateString() <= hariBooking.toDateString()) {
        console.log("Mobil Ke-" + i);
        console.log("Hari Booking: " + hariBooking.toDateString());
        console.log("Batas Tersedia: " + batasTersedia.toDateString());
        i++;
      }
      if (
        car.capacity == params.jumlahPenumpang &&
        batasTersedia.toDateString() <= hariBooking.toDateString()
      ) {
        const node = document.createElement("span");
        node.classList.add("col");
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
      }
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
