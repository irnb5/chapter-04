const path = require("path");
const express = require("express");
const app = express();

app.use(express.static(__dirname + "public"));

app.get("/", function (_req, res) {
  res.sendFile(path.join(__dirname + "../public/index.html"));
});

app.listen(3000);
console.log("Serving at localhost:3000");
