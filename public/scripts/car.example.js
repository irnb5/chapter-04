class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `

              <div class="card mb-4" style="width:333px">
                <img
                  src="${this.image}"
                  class="card-img-top"
                  style="height:222px"
                  alt="..."
                />
                <div class="card-body">
                  <h5 class="card-title">${
                    this.manufacture + " " + this.model
                  } </h5>
                  <h5><strong>${new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                  }).format(this.rentPerDay)} /hari</strong></h5>
                  <p class="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <p class="card-text">
                   <img class="me-2 mb-1" src="images/fi_users.svg" alt = "imgtemplate"></img>${
                     this.capacity
                   } Orang
                  </p>
                  
                  <p class="card-text">
                   <img class="me-2" src="images/transmition.svg" alt = "imgtemplate"></img> ${
                     this.transmission
                   }
                  </p>
                  <p class="card-text">
                    Batas Tersedia: ${this.availableAt}
                  </p>
                  <p class="card-text">
                    <img class="me-2" src="images/tahunproduksi.svg" alt = "imgtemplate"></img>${
                      this.year
                    }
                  </p>
                  
                  
                  <a href="#" class="btn btn-success btn-lg w-100">Pilih Mobil</a>
                </div>
              </div>

    `;
  }
  //   <p>id: <b>${this.id}</b></p>
  // <p>plate: <b>${this.plate}</b></p>
  // <p>manufacture: <b>${this.manufacture}</b></p>
  // <p>model: <b>${this.model}</b></p>
  // <p>available at: <b>${this.availableAt}</b></p>
  // <img src="${this.image}" alt="${this.manufacture}" width="64px"></img>
}
